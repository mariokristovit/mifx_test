import axios from 'axios';
import React, {useEffect, useState} from 'react';
import {
  Text,
  View,
  StyleSheet,
  StatusBar,
  ScrollView,
  FlatList,
  Image,
} from 'react-native';
import {Rating} from 'react-native-ratings';
import Icon2 from 'react-native-vector-icons/EvilIcons';
import Icon from 'react-native-vector-icons/Ionicons';
import styleUi from '../style/styleUi';

const MainMenu = () => {
  const [categoryData, setCategoryData] = useState([]);
  const [productData, setProductData] = useState([]);
  const [token, setToken] = useState('');

  useEffect(() => {
    //REQUEST LOGIN
    axios
      .post(
        `https://fe.dev.dxtr.asia/api/login?email=user@test.io&password=Test123.`,
      )
      .then(response => {
        setToken(response.data.token);
      })
      .catch(function (error) {
        console.log(error);
      });
  }, []);

  useEffect(() => {
    console.log('INI ADALAH TOKEN BARU : ', token);
    if (token) {
      //REQUEST CATEGORY
      axios
        .get(`https://fe.dev.dxtr.asia/api/category`, {
          headers: {Authorization: `Bearer ${token}`},
        })
        .then(response => {
          console.log('INI ADALAH CATEGORY : ', response.data);
          setCategoryData(response.data);
        })
        .catch(function (error) {
          console.log('ERROR CATEGORY: ', error);
        });
      //REQUEST PRODUCT
      axios
        .get(`https://fe.dev.dxtr.asia/api/products`, {
          headers: {Authorization: `Bearer ${token}`},
        })
        .then(response => {
          console.log('INI ADALAH PRODUCT : ', response.data);
          setProductData(response.data);
        })
        .catch(function (error) {
          console.log('ERROR PRODUCT: ', error);
        });
    }
  }, [token]);

  return (
    <View style={styleUi.container}>
      <StatusBar backgroundColor="#f7f7f7" barStyle="dark-content" />
      <View style={styleUi.topBarContainer}>
        <View style={styleUi.backIcon}>
          <Icon name="arrow-back" size={40} color="black" />
        </View>
        <View style={styleUi.titleContainer}>
          <Text style={styleUi.title}>Shoes</Text>
        </View>
        <View
          style={styleUi.filterContainer}>
          <Icon name="filter" size={40} color="black" />
        </View>
      </View>
      <View style={styleUi.categoryContainer}>
        <ScrollView horizontal>
          {categoryData.map((item, index) => (
            <View
              key={index}
              style={styleUi.scrollViewContainer}>
              <Text style={styleUi.category}>
                {item.name}
              </Text>
            </View>
          ))}
        </ScrollView>
      </View>
      <View
        style={styleUi.mainContentContainer}>
        <FlatList
          numColumns={2}
          data={productData}
          keyExtractor={(item, index) => index.toString()}
          renderItem={({item, index}) => (
            <View style={index % 2 == 0 ? styleUi.oddContent : styleUi.evenContent}>
              <View
                style={styleUi.avaliableContent}>
                <View style={item.new ? styleUi.newContent : styleUi.outStockContent}>
                  <Text style={item.new ? styleUi.textNewContent : styleUi.textNewContent}>
                    {item.new ? 'New' : 'Out of Stock'}
                  </Text>
                </View>
                <Icon2 name="heart" size={20} color="black" />
              </View>
              <View style={{alignItems: 'center'}}>
                <Image
                  style={styleUi.imageContent}
                  source={{
                    uri: item.image,
                  }}
                />
              </View>
              <Rating
                readonly
                type="star"
                ratingColor="black"
                ratingBackgroundColor="white"
                ratingCount={5}
                startingValue={item.rating}
                imageSize={10}
                style={styleUi.ratingContent}
              />
              <Text style={styleUi.productName}>
                {item.name}
              </Text>
              <View
                style={styleUi.bottomContentContainer}>
                <Text style={styleUi.priceProduct}>
                  {item.price}
                </Text>
                <Text style={{color: 'blue'}}>{item.off}</Text>
              </View>
            </View>
          )}
        />
      </View>
    </View>
  );
};

export default MainMenu;
