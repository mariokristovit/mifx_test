## Name
MIFX Test Project

## Description
This application made for job test purpose at MIFX.
The framework used for making this application is React Native.

## Installation
After running the clone process, run command "npm install" in project directory first, then run the command "react-native run-android" or "npx react-native run-android"

## Usage
After instaling process, just open the MIFX_test app.

## Support
Email: mariokristovit@gmail.com
Gitlab: https://gitlab.com/mariokristovit

## Authors
@mariokristov

## Project status
Active(Waiting for MIFX's Team Review)
